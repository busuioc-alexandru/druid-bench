<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/3/17
 * Time: 10:13 AM
 */

namespace DruidBench;

use Druid\Query\AbstractQuery;
use Druid\Query\Aggregation\AbstractAggregationQuery;
use Druid\Query\Component\Granularity\PeriodGranularity;
use Druid\Query\Component\Granularity\SimpleGranularity;
use Druid\Query\Component\Metric\NumericTopNMetric;
use Druid\Query\QueryInterface;
use Druid\QueryBuilder\AbstractAggregationQueryBuilder;
use Druid\QueryBuilder\TimeseriesQueryBuilder;
use Druid\QueryBuilder\TopNQueryBuilder;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\TransferStats;

class Common
{
    const QUERY_QUERY = 2;
    const QUERY_RESULT = 3;
    const QUERY_ERROR = 4;
    const QUERY_STATS = 5;
    const ABSOLUTE = 'ABS';
    const RELATIVE = 'REL';
    /** @var PromiseInterface[] */
    public static $promises = [];
    /** @var QueryInterface[] */
    public static $queries;
    /** @var array[] */
    private static $stats;
    /** @var string $qpid The Query Path ID - a dir name placed under /app/queries,
     * that will host all data relaterd to the current process */
    private static $qpid;

    /**
     * Returns the TimeriesQueryBuilder, without the required 'intervals' and 'granularity';
     * which will be set later
     * @return TimeseriesQueryBuilder
     */
    public static function getNewTimeseriesQueryBuilder()
    {
        $druid = Di::getDruid();

        /** @var TimeseriesQueryBuilder $builder */
        $builder = $druid->createQueryBuilder(AbstractAggregationQuery::TYPE_TIMESERIES);
        $builder
            ->setDataSource(Di::getUnionDatasource())
            ->setFilter($builder->filter()->selectorFilter('idaffiliate', 8741))
            ->setDescending(true);

        Common::setGenericAggregations($builder);

        return $builder;
    }

    /**
     * @param AbstractAggregationQueryBuilder $builder
     */
    public static function setGenericAggregations(AbstractAggregationQueryBuilder $builder)
    {
        $Clicks = $builder->postAggregator()->fieldAccessPostAggregator('Clicks', 'Clicks');
        $Visits = $builder->postAggregator()->fieldAccessPostAggregator('Visits', 'Visits');
        $Signups = $builder->postAggregator()->fieldAccessPostAggregator('Signups', 'Signups');
        $Revenue = $builder->postAggregator()->fieldAccessPostAggregator('Revenue', 'Revenue');

        $builder
            ->addAggregator($builder->aggregator()->longSum('Clicks', 'clicks'))
            ->addAggregator($builder->aggregator()->doubleSum('Revenue', 'revenue'))
            ->addAggregator($builder->aggregator()->longSum('Signups', 'sales'))
            ->addAggregator($builder->aggregator()->doubleSum('Visits', 'visits'))
            ->addPostAggregator($builder->postAggregator()->arithmeticPostAggregator('CTR', '/', [$Clicks, $Visits]))// Click Through Rate (x100 in views)
            ->addPostAggregator($builder->postAggregator()->arithmeticPostAggregator('CR', '/', [$Signups, $Clicks]))// Conversion rate
            ->addPostAggregator($builder->postAggregator()->arithmeticPostAggregator('EPC', '/', [$Revenue, $Clicks]))// Earnings Per Click (x100 in views)
            ->addPostAggregator($builder->postAggregator()->arithmeticPostAggregator('AP', '/', [$Revenue, $Signups]))// Average payout
            ->addPostAggregator($builder->postAggregator()->constantPostAggregator('Cost', 0))
            ->addPostAggregator($builder->postAggregator()->constantPostAggregator('Profit', 0));
    }

    /**
     * Returns the TimeriesQueryBuilder, without the required 'intervals' and 'granularity';
     * which will be set later
     * @param array $filter
     * @return TopNQueryBuilder
     */
    public static function getNewTopNQueryBuilder(array $filter = [])
    {
        $druid = Di::getDruid();

        /** @var TopNQueryBuilder $builder */
        $builder = $druid->createQueryBuilder(AbstractAggregationQuery::TYPE_TOP_N);
        $builder
            ->setDataSource(Di::getUnionDatasource())
            ->setDimension('country_code', 'CountryId')
            ->setThreshold(5000)
            ->setMetric(new NumericTopNMetric('Revenue'));
        if (!$filter) $filter = ['idaffiliate' => 8741];
        if ($filter) {
            $qbFilter = [];
            array_walk($filter, function ($value, $fieldName) use ($builder, &$qbFilter) {
                $qbFilter[] = $builder->filter()->selectorFilter($fieldName, $value);
            });
            $builder
                ->setFilter($builder->filter()->andFilter($qbFilter));
        }

        Common::setGenericAggregations($builder);

        return $builder;
    }

    public static function persist(AbstractAggregationQuery $query)
    {
        $destination = self::getQueryFilepath($query);
        $prettyJson = Common::getPrettyJson(Common::getJsonQuery($query));

        if (!file_put_contents($destination, $prettyJson)) {
            throw new \Exception(sprintf('Cannot save to [%s]', $destination));
        }

        return $destination;
    }

    public static function getQueryFilepath(QueryInterface $query, $destination = self::QUERY_QUERY, $path = self::ABSOLUTE)
    {
        $prefix = $path == self::ABSOLUTE ? self::getBenchResultsPath() : '';
        return implode(DIRECTORY_SEPARATOR, [
            $prefix,
            self::getQueriesPathId(),
            self::getQueryFilename($query, $destination)
        ]);
    }

    private function getBenchResultsPath()
    {
        return APP_PATH . '/app/bench-results';
    }

    private static function getQueriesPathId()
    {
        if (!self::$qpid) {
            self::$qpid = date('Y-m-d H-i-s');
            mkdir(self::getBenchResultsPath() . DIRECTORY_SEPARATOR . self::$qpid, 0777, true);
        }
        return self::$qpid;
    }

    public static function getQueryFilename(QueryInterface $query, $destination = self::QUERY_QUERY)
    {
        /** @var AbstractAggregationQuery $query */
        $query_type = $query->getQueryType();
        $granularity = $query->getGranularity();
        if ($granularity instanceof SimpleGranularity) $granularity = sprintf('gran-%s', $granularity->getGranularity());
        elseif ($granularity instanceof PeriodGranularity) $granularity = sprintf('gran-%s-%s-%s', $granularity->getType(), $granularity->getPeriod(), $granularity->getTimeZone());
        else $granularity = $granularity = sprintf('gran-type-%s', $granularity->getType());
        $intervals = $query->getIntervals();
        $intervals = array_reduce($intervals, function ($carry, $item) {
            return $carry . (string)$item;
        }, '');
        switch ($destination) {
            case self::QUERY_ERROR:
                $append = 'error';
                break;
            case self::QUERY_RESULT:
                $append = 'result';
                break;
            case self::QUERY_STATS:
                $append = 'stats';
                break;
            default:
                $append = 'query';
                break;
        }
        return str_replace([DIRECTORY_SEPARATOR, ':'], '-', sprintf('%s-%s-%s-%s.json', $query_type, $granularity, $intervals, $append));
    }

    /**
     * @param string $json
     * @return string
     */
    public static function getPrettyJson($json)
    {
        $arr = \json_decode($json);
        return json_encode($arr, JSON_PRETTY_PRINT);
    }

    /**
     * @param AbstractQuery $query
     * @return string
     */
    public static function getJsonQuery(AbstractQuery $query)
    {
        $serializer = Di::getSerializer();
        $json = $serializer->serialize($query, 'json');
        return (string)$json;
    }

    public static function persistResult(\GuzzleHttp\Psr7\Response $response, QueryInterface $query)
    {
        $filepath = self::getQueryFilepath($query, self::QUERY_RESULT);
        file_put_contents($filepath, $response->getBody());
    }

    public static function persistException(\RuntimeException $response, QueryInterface $query)
    {
        /** @var ServerException $response */
        /** @var \GuzzleHttp\Psr7\Response $gResponse */
        $gResponse = $response->getResponse();
        $fillResponse = $gResponse ? $gResponse->getBody()->getContents() : \json_encode([
            'message' => $response->getMessage(),
            'file' => $response->getFile(),
            'line' => $response->getLine(),
        ], JSON_PRETTY_PRINT);
        $filepath = self::getQueryFilepath($query, self::QUERY_ERROR);
        file_put_contents($filepath, $fillResponse ?: $response->getMessage());
    }

    public static function pushStats(TransferStats $stats)
    {
        if ($request = $stats->getRequest()) {
            $header = $request->getHeaderLine(Pool::HEADER_NAME_QUERY_ID);
            self::$stats[$header] = self::prepareStats($stats);
        } elseif ($stats->getResponse()) {
            self::$stats[spl_object_hash($stats->getResponse())] = self::prepareStats($stats);
        }
    }

    public static function prepareStats(TransferStats $stats)
    {
        $response = $stats->getResponse();
        return [
            'hasResponse' => $stats->hasResponse(),
            'transferTime' => $stats->getTransferTime(),
            'response' => $response ? [
                'statusCode' => $response->getStatusCode(),
                'length' => $response->getBody()->getSize(),
                'headers' => $response->getHeaders(),
            ] : null
        ];

    }

    public static function linkLogStats()
    {
        foreach (Common::$queries as $requestHash => $query) {
            // log only successful requests
            array_key_exists($requestHash, self::$stats) &&
            Common::logStats($query, self::$stats[$requestHash]);
        }
    }

    public static function logStats(QueryInterface $query, array $stats)
    {
        $filepath = self::getQueryFilepath($query, self::QUERY_STATS);
        file_put_contents($filepath, \json_encode($stats, JSON_PRETTY_PRINT));
    }

    public static function getQueryRequestTransferTime($query)
    {
        $queryHash = array_search($query, self::$queries);
        return array_key_exists($queryHash, self::$stats) ? self::$stats[$queryHash]['transferTime'] : 0;
    }
}
