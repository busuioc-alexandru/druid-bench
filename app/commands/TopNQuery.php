<?php

namespace DruidBench\Command;

use Druid\Query\Component\GranularityInterface;
use DruidBench\Common;
use DruidBench\Pool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TopNQuery extends Command
{
    /** @var array */
    private $storage = [];
    /** @var OutputInterface */
    private $output;

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('druid:topn-query')
            // the short description shown while running "php bin/console list"
            ->setDescription('Runs TopN Query against Druid.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Runs TopN Query against Druid.");
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln('Prepare Druid Queries');
        $this->output = $output;

        $this->setupQuery(
            new \DateTime('midnight first day of last month'),
            (new \DateTime('midnight first day of this month'))->modify('-1 hour'),
            'all'
        );

        $this->setupQuery(
            new \DateTime('midnight first day of 2 months ago', new \DateTimeZone('UTC')),
            (new \DateTime('midnight first day of 1 month ago', new \DateTimeZone('UTC')))->modify('-1 hour'),
            'day'
        );

        $this->setupQuery(
            new \DateTime('yesterday midnight', new \DateTimeZone('UTC')),
            (new \DateTime('midnight', new \DateTimeZone('UTC')))->modify('-1 hour'),
            'all'
        );

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getParameterOption('collect')) {
            Pool::collect($this->storage);
        } else {
            Pool::runAll($this->storage, $output);
        }
    }

    /**
     * @param \DateTime $startInterval
     * @param \DateTime $endInterval
     * @param string|GranularityInterface $granularity
     */
    private function setupQuery( \DateTime $startInterval, \DateTime $endInterval, $granularity )
    {
        $builder = Common::getNewTopNQueryBuilder();
        $builder
            ->addInterval($startInterval, $endInterval)
            ->setGranularity($granularity);
        $query = $builder->getQuery();
        $query->validate();
        $queryPersistencePath = Common::persist($query);

        $this->output->writeln(sprintf(' > TopN Query P1D is available at %s', $queryPersistencePath));
        $this->storage[] = $query;
    }
}
