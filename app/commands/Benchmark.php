<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/6/17
 * Time: 10:08 AM
 */

namespace DruidBench\Command;

use DruidBench\Pool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Benchmark extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('benchmark')
            // the short description shown while running "php bin/console list"
            ->setDescription('Runs Benchmark with all the registered Queries against Druid.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Runs Benchmark with all the registered Queries against Druid.");
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $queries = $this->getApplication()->all('druid');
        array_walk($queries, function (Command $command) use ($output) {
            $output->writeln(sprintf('Initializing the Queries under Command type [%s]', get_class($command)));
            $command->initialize(new ArrayInput([]), $output);
        });
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queries = $this->getApplication()->all('druid');
        array_walk($queries, function (Command $command) use ($output) {
            $output->writeln(sprintf('Collect Queries from %s', get_class($command)));
            $command->execute(new ArrayInput(['collect' => true]), $output);
        });
        Pool::runAll([], $output);
    }
}