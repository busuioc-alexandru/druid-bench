#!/usr/bin/env php
<?php

use DruidBench\Command\Benchmark;
use DruidBench\Command\TimeseriesQuery;
use DruidBench\Command\TopNQuery;
use Symfony\Component\Console\Application;

define('APP_PATH', dirname(__DIR__));

error_reporting(E_ERROR);
date_default_timezone_set('UTC');

require APP_PATH . '/vendor/autoload.php';

$application = new Application();

// register commands
$application->add(new Benchmark());
$application->add(new TimeseriesQuery());
$application->add(new TopNQuery());

// cleanup the existing entries
//$finder = new \Symfony\Component\Finder\Finder();
//$finder->files()->in(APP_PATH . '/app/queries')->filter(function (\SplFileInfo $info) {
//    return $info->getExtension() == 'json';
//});
//foreach ($finder as $filepath) {
//    /** @var \SplFileInfo $filepath */
//    unlink($filepath);
//}

$application->run();
