<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09-02-2017
 * Time: 11:07
 */

namespace DruidBench;

use Druid\Query\QueryInterface;
use GuzzleHttp\Pool as GuzzlePool;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;
use Symfony\Component\Console\Output\OutputInterface;


class Pool
{
    const HEADER_NAME_QUERY_ID = 'X-Query-Id';
    /** @var QueryInterface[] */
    private static $queries;

    /**
     * @param QueryInterface[] $queries
     * @param OutputInterface $output
     */
    public static function runAll(array $queries, OutputInterface $output)
    {
        $start = microtime(true);

        self::collect($queries);

        $client = Di::getGuzzleClient();
        $serializer = Di::getSerializer();
        $config = Di::getConfig();

        $requests = function (array $queries) use ($client, $serializer, $output) {
            foreach ($queries as $queryHash => $query) {
                $output->write(sprintf(' > Sending the [%s #%s]... ', get_class($query), spl_object_hash($query)));
                $body = $serializer->serialize($query, 'json');
                Common::$queries[$queryHash] = $query;
                $request = function ($poolOpts) use ($client, $queryHash, $body) {
                    $reqOpts = [
                        'body' => $body,
                        'headers' => [
                            self::HEADER_NAME_QUERY_ID => $queryHash,
                            'Content-Type' => 'application/json'
                        ],
                    ];
                    if (is_array($poolOpts) && count($poolOpts) > 0) {
                        $reqOpts = array_merge($poolOpts, $reqOpts);
                    }

                    return $client->postAsync('', $reqOpts);
                };
                $output->writeln('OK!');
                yield $request;
            }
        };

        $pool = new GuzzlePool($client, $requests(self::$queries), [
            'concurrency' => $config['connection']['concurrency'],
            'fulfilled' => function ($response, $index/*, $promise, $x*/) use ($output) {
                $query = self::$queries[array_keys(self::$queries)[$index]];
                Common::persistResult($response, $query);
                $message = sprintf("%s for query #%s [%.4f s] saved in %s", get_class($response), spl_object_hash($query), Common::getQueryRequestTransferTime($query), Common::getQueryFilepath($query, Common::QUERY_RESULT, Common::RELATIVE));
                $output->writeln($message);
            },
            'rejected' => function ($reason, $index/*, $promise, $x*/) use ($output) {
                $query = self::$queries[array_keys(self::$queries)[$index]];
                Common::persistException($reason, $query);
                $message = sprintf("%s #%s [%.4f s] saved in %s", get_class($reason), spl_object_hash($query), Common::getQueryRequestTransferTime($query), Common::getQueryFilepath($query, Common::QUERY_ERROR, Common::RELATIVE));
                $output->writeln($message);
            },
            'options' => [
                RequestOptions::ON_STATS => function (TransferStats $stats) {
                    Common::pushStats($stats);
                }
            ]
        ]);
        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        Common::linkLogStats();
        $output->writeln(sprintf("\n\nDuration: %.2fs\n\n", microtime(true) - $start));
    }

    public static function collect(array $queries)
    {
        array_walk($queries, function (QueryInterface $query) {
            self::$queries[spl_object_hash($query)] = $query;
        });
    }


}
