<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/2/17
 * Time: 11:29 AM
 */

namespace DruidBench;


use Doctrine\Common\Annotations\AnnotationRegistry;
use Druid\Driver\ConnectionConfig;
use Druid\Driver\Guzzle\Driver;
use Druid\Druid;
use Druid\Query\Component\DataSource\UnionDataSource;
use GuzzleHttp\Client;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class Di
{

    private static $container = [];
    private static $output;

    /**
     * @return Druid
     */
    public static function getDruid()
    {
        if (!array_key_exists('druid', self::$container)) {

            AnnotationRegistry::registerAutoloadNamespace(
                'JMS\Serializer\Annotation',
                __DIR__ . '/../vendor/jms/serializer/src'
            );

            self::$container['druid'] = new Druid(new Driver(), self::getDruidParams());
        }
        return self::$container['druid'];
    }

    private function getDruidParams()
    {
        $config = self::getConfig();
        return [
            'scheme' => $config['druid']['scheme'],
            'host' => $config['druid']['host'],
            'port' => $config['druid']['port'],
            'path' => $config['druid']['path'],
            'proxy' => $config['druid']['proxy'], // default null
            'timeout' => $config['druid']['timeout'], // in seconds - default null
        ];
    }

    public static function getConfig()
    {
        if (!array_key_exists('config', self::$container)) {
            self::$container['config'] = Yaml::parse(file_get_contents(__DIR__ . '/config.yaml'));
        }
        return self::$container['config'];
    }

    /**
     * @return UnionDataSource
     */
    public static function getUnionDatasource()
    {
        if (!array_key_exists('union-datasource', self::$container)) {
            self::$container['union-datasource'] = new UnionDataSource(['ola-redirects', 'dedup-sales']);
        }
        return self::$container['union-datasource'];
    }

    /**
     * @return Serializer
     */
    public static function getSerializer()
    {
        if (!array_key_exists('serializer', self::$container)) {

            AnnotationRegistry::registerAutoloadNamespace(
                'JMS\Serializer\Annotation',
                __DIR__ . '/../vendor/jms/serializer/src'
            );

            self::$container['serializer'] = SerializerBuilder::create()
                ->setPropertyNamingStrategy(new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy()))
                ->build();
        }
        return self::$container['serializer'];
    }

    /**
     * @return Client
     */
    public static function getGuzzleClient()
    {
        if (!array_key_exists('client', self::$container)) {

            $config = new ConnectionConfig(self::getDruidParams());
            $params = ['base_uri' => $config->getBaseUri()];
            if ($proxy = $config->getProxy()) {
                $params['proxy'] = $proxy;
            }
            if ($timeout = $config->getTimeout()) {
                $params['timeout'] = $timeout;
            }

            self::$container['client'] = new Client($params);
        }
        return self::$container['client'];
    }

    public static function getOutput()
    {
        return self::$output ?: (self::$output = new ConsoleOutput());
    }

    public static function setOutput(OutputInterface $output)
    {
        self::$output = $output;
    }
}
